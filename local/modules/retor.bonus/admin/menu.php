<?php

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use retor\bonus\Helper\table\TableEditHelper;
use retor\bonus\Helper\table\TableListHelper;
use retor\bonus\Helper\transact\TransactEditHelper;
use retor\bonus\Helper\transact\TransactListHelper;

Loc::loadMessages(__FILE__);

$menu = array(
    array(
        'parent_menu' => 'global_menu_store',
        'sort' => 100,
        'text' => Loc::getMessage('RETOR_BONUS_MENU_TITLE'),
        'title' => Loc::getMessage('RETOR_BONUS_MENU_TITLE'),
        'url' => 'retor_index.php',
        "icon"        => "retor_bonus_menu_icon", // малая иконка
        "page_icon"   => "retor_bonus_page_icon", // большая иконка
        'items_id' => 'menu_bonus',
        'items' => array(
            array(
                'text' => Loc::getMessage('RETOR_BONUS_ACCOUNTS_TITLE'),
                'title' => Loc::getMessage('RETOR_BONUS_ACCOUNTS_TITLE'),
                "url" => TableEditHelper::getListPageURL(),
                "more_url" => array(
                    TableListHelper::getEditPageURL()
                ),
            ),
            array(
                'text' => Loc::getMessage('RETOR_BONUS_TRANSACT_TITLE'),
                'title' => Loc::getMessage('RETOR_BONUS_TRANSACT_TITLE'),
                "url" => TransactEditHelper::getListPageURL(),
                "more_url" => array(
                    TransactListHelper::getEditPageURL()
                ),
            ),
        ),
    ),
);

return $menu;
