<? if ((!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) && !isset($_POST["AJAX_CALL"])) die();

global $USER;

$arResult = array();

$CanPay = false;

$ORDER_ID = $arParams['ORDER_ID'];

$arResult['ACCOUNT'] = $Account = CBonusAccount::getAccountByUserID($USER->GetID()); //получаем аккаунт пользователя
$ORDER = \CSaleOrder::GetByID($ORDER_ID);

if($ORDER['SUM_PAID'] == 0)
{
    $CanPay = true;
}


$RETOR_BONUS_MAX_PERCENT = COption::GetOptionString("retor.bonus", "retor_bonus_max_percent");
$half_price = ($ORDER['PRICE'] * intval($RETOR_BONUS_MAX_PERCENT)) / 100;

$arResult['AVALIBLE_SUM'] = CBonusAccount::getBonusRemain($ORDER_ID,$half_price);

$arResult['ORDER'] = $ORDER;
$arResult['CanPay'] = $CanPay;


$this->IncludeComponentTemplate();

?>

