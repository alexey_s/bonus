<?php

require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

	$BONUS_NEED = $_REQUEST['BONUS_NEED'];
	$ORDER_ID = $_REQUEST['ORDER_ID'];

	if($BONUS_NEED > 0 && $ORDER_ID > 0){
		$Remain = CBonusAccount::getBonusRemain($ORDER_ID,$BONUS_NEED);

		if($Remain){

			$result = CBonusAccount::PayOut($ORDER_ID,$Remain,true);

		}
	}

echo json_encode(array('result' => $result));

?>

