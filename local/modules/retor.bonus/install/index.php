<?php

use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;
use retor\bonus\model\AccountsTable;
use retor\bonus\model\ProgramTable;
use retor\bonus\model\TransactTable;


Loc::loadMessages(__FILE__);

class retor_bonus extends CModule
{
    var $MODULE_CSS;

    public function __construct()
    {
        $arModuleVersion = array();

        include __DIR__ . '/version.php';

        if (is_array($arModuleVersion) && array_key_exists('VERSION', $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion['VERSION'];
            $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        }

        $this->MODULE_ID = 'retor.bonus';
        $this->MODULE_NAME = Loc::getMessage('RETOR_BONUS_MODULE_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('RETOR_BONUS_MODULE_DESCRIPTION');
        $this->MODULE_GROUP_RIGHTS = 'Y';
        $this->PARTNER_NAME = Loc::getMessage('RETOR_BONUS_MODULE_PARTNER_NAME');
        $this->PARTNER_URI = 'http://retor.ru';
    }

    public function installDB()
    {
        global $APPLICATION, $DB;

        AccountsTable::getEntity()->createDbTable();
        ProgramTable::getEntity()->createDbTable();
        TransactTable::getEntity()->createDbTable();


        $sqlFiles = array(
            'retor_bonus_accounts_table.sql',
            'retor_bonus_program_table.sql',
            'retor_bonus_transact_table'
        );


        foreach ($sqlFiles as $file) {

            $this->errors = $DB->RunSQLBatch(__DIR__ . '/db/' . strtolower($DB->type) . '/' . $file);
            $APPLICATION->ResetException();
            if ($this->errors !== false) {
                $APPLICATION->ThrowException(implode("<br>", $this->errors));
                return false;
            }
        }

        return true;
    }

    public function InstallFiles()
    {
        copyDirFiles(
            __DIR__ . '/admin',
            $_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin',
            true, true
        );

        CopyDirFiles(
            __DIR__ .'/themes',
            $_SERVER["DOCUMENT_ROOT"].'/bitrix/themes',
            true, true
        );

        CopyDirFiles(
            __DIR__ .'/components',
            $_SERVER["DOCUMENT_ROOT"]."/local/components",
            true, true);


        return true;
    }

    function UnInstallFiles()
    {
        DeleteDirFilesEx($_SERVER["DOCUMENT_ROOT"]."/local/components/retor/bonus.list/");
        DeleteDirFilesEx($_SERVER["DOCUMENT_ROOT"]."/local/components/retor/bonus.payment/");
        return true;
    }

    public function uninstallDB()
    {
        if (Loader::includeModule($this->MODULE_ID)) {
            $connection = Application::getInstance()->getConnection();
            $connection->dropTable(AccountsTable::getTableName());
            $connection->dropTable(ProgramTable::getTableName());
            $connection->dropTable(TransactTable::getTableName());
        }
    }

    function addPostType($EVENT_NAME, $NAME, $LID, $DESCRIPTION)
    {
        $res = false;
        $arFilter = array(
            "TYPE_ID" => $EVENT_NAME, //"RETOR_BONUS_CHANGE",
            "LID" => $LID
        );

        $rsET = CEventType::GetList($arFilter);
        $arET = $rsET->Fetch();

        if (!$arET) {
            $obEventType = new CEventType;
            $res = $obEventType->Add(array(
                "LID"           => $LID,
                "EVENT_NAME"    => $EVENT_NAME,
                "NAME"          => $NAME,
                "DESCRIPTION"   => $DESCRIPTION
            ));
        }

        return $res;
    }

    function delPostType($EVENT_NAME)
    {
        $et = new CEventType;
        $res = $et->Delete($EVENT_NAME);
        return $res;
    }

    /*
    $arr["ACTIVE"] = "Y";
    $arr["EVENT_NAME"] = "ADV_CONTRACT_INFO";
    $arr["LID"] = array("s6");
    $arr["EMAIL_FROM"] = "#DEFAULT_EMAIL_FROM#";
    $arr["EMAIL_TO"] = "#EMAIL_TO#";
    $arr["BCC"] = "#BCC#";
    $arr["SUBJECT"] = "Тема сообщения";
    $arr["BODY_TYPE"] = "text";
    $arr["MESSAGE"] = "Текст сообщения";
    */
    function addPostTemplate($arFields)
    {
        $emess = new CEventMessage;
        $res = $emess->Add($arFields);

        return $res;
    }

    function delPostTemplate($TEMPLATE_ID)
    {
        global $DB;
        $res = false;

        if(intval($TEMPLATE_ID)>0)
        {
            $emessage = new CEventMessage;
            $DB->StartTransaction();
            if(!$emessage->Delete(intval($TEMPLATE_ID)))
            {
                $DB->Rollback();
            }
            else {
                $DB->Commit();
                $res = true;
            }
        }

        return $res;
    }

    public function doInstall()
    {
        ModuleManager::registerModule($this->MODULE_ID);
        \Bitrix\Main\Loader::includeModule($this->MODULE_ID);
        $this->installDB();
        $this->InstallFiles();

        self::addPostType("RETOR_BONUS_INC_BALL","Начисление баллов на счет пользователя","ru","#USERNAME# - имя пользователя,#BONUS_DATE# - дата начисления,#BONUS_SUM# - счет пользователя,
         #BONUS_INC# - величина начисления, #ORDER_ID# - номер заказа, #ORDER_DATE# - дата заказа, #ORDER_SUM# - сумма заказа");

        self::addPostType("RETOR_BONUS_DEC_BALL","Списание баллов со счета пользователя","ru","#USERNAME# - имя пользователя,#BONUS_DATE# - дата списания,#BONUS_SUM# - счет пользователя,
         #BONUS_DEC# - величина списания, #ORDER_ID# - номер заказа, #ORDER_DATE# - дата заказа, #ORDER_SUM# - сумма заказа");

        $arr["ACTIVE"] = "Y";
        $arr["EVENT_NAME"] = "RETOR_BONUS_INC_BALL";
        $arr["LID"] = array("s6");
        $arr["EMAIL_FROM"] = "#DEFAULT_EMAIL_FROM#";
        $arr["EMAIL_TO"] = "#EMAIL_TO#";
        $arr["BCC"] = "#BCC#";
        $arr["SUBJECT"] = "Начисление баллов на счет пользователя #USERNAME#";
        $arr["BODY_TYPE"] = "text";
        $arr["MESSAGE"] = "Уважаемый #USERNAME#, #BONUS_DATE# Вам начилено #BONUS_INC# баллов за заказ № #ORDER_ID# #ORDER_DATE#";

        self::addPostTemplate($arr);

        $arr["ACTIVE"] = "Y";
        $arr["EVENT_NAME"] = "RETOR_BONUS_DEC_BALL";
        $arr["LID"] = array("s6");
        $arr["EMAIL_FROM"] = "#DEFAULT_EMAIL_FROM#";
        $arr["EMAIL_TO"] = "#EMAIL_TO#";
        $arr["BCC"] = "#BCC#";
        $arr["SUBJECT"] = "Списание баллов со счета пользователя #USERNAME#";
        $arr["BODY_TYPE"] = "text";
        $arr["MESSAGE"] = "Уважаемый #USERNAME#, #BONUS_DATE# с Вашего счета списано #BONUS_DEC# баллов при оплате заказа № #ORDER_ID# #ORDER_DATE#";

        self::addPostTemplate($arr);

        $eventManager = \Bitrix\Main\EventManager::getInstance();
        $eventManager->registerEventHandler(
            'main',
            'OnPageStart',
            $this->MODULE_ID,
            '\retor\bonus\EventHandlers',
            'onPageStart'
        );

        $eventManager->registerEventHandler(
            'sale',
            'OnSaleStatusOrder',
            $this->MODULE_ID,
            '\retor\bonus\EventHandlers',
            'OnSaleStatusOrder'
        );

        $eventManager->registerEventHandler(
            'sale',
            'OnSaleComponentOrderResultPrepared',
            $this->MODULE_ID,
            '\retor\bonus\EventHandlers',
            'OnSaleComponentOrderResultPrepared'
        );

        $eventManager->registerEventHandler(
            'sale',
            'OnSaleOrderSaved',
            $this->MODULE_ID,
            '\retor\bonus\EventHandlers',
            'OnSaleOrderSaved'
        );



    }

    public function doUninstall()
    {

        self::delPostType("RETOR_BONUS_INC_BALL");
        self::delPostType("RETOR_BONUS_DEC_BALL");

        $arFilter = Array(
            "TYPE_ID"       => array("RETOR_BONUS_INC_BALL", "RETOR_BONUS_DEC_BALL"),
        );

        $rsMess = CEventMessage::GetList($by="site_id", $order="desc", $arFilter);

        while($arMess = $rsMess->GetNext())
        {
            self::delPostTemplate($arMess["ID"]);
        }

        $eventManager = \Bitrix\Main\EventManager::getInstance();


        $eventManager->unRegisterEventHandler(
            'main',
            'OnPageStart',
            $this->MODULE_ID,
            '\retor\bonus\EventHandlers',
            'onPageStart'
        );

        $eventManager->unRegisterEventHandler(
            'sale',
            'OnSaleStatusOrder',
            $this->MODULE_ID,
            '\retor\bonus\EventHandlers',
            'OnSaleStatusOrder'
        );

        $eventManager->registerEventHandler(
            'sale',
            'OnSaleComponentOrderResultPrepared',
            $this->MODULE_ID,
            '\retor\bonus\EventHandlers',
            'OnSaleComponentOrderResultPrepared'
        );

        $eventManager->registerEventHandler(
            'sale',
            'OnSaleOrderSaved',
            $this->MODULE_ID,
            '\retor\bonus\EventHandlers',
            'OnSaleOrderSaved'
        );

        $this->uninstallDB();
        $this->UnInstallFiles();
        ModuleManager::unRegisterModule($this->MODULE_ID);


    }


}
