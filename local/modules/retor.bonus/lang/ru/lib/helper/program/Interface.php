<?php
/**
 * Created by PhpStorm.
 * User: devaccess
 * Date: 15.07.17
 * Time: 20:15
 */

$MESS['PROGRAM_TABLE_ENTITY_ID_FIELD'] = 'ID';
$MESS['PROGRAM_TABLE_ENTITY_ACTIVE_FIELD'] = 'Активность';
$MESS['PROGRAM_TABLE_ENTITY_SITE_FIELD'] = 'Сайт';
$MESS['PROGRAM_TABLE_ENTITY_PROGRAM_NAME_FIELD'] = 'Название программы';
$MESS['PROGRAM_TABLE_ENTITY_USER_GROUP_ID_FIELD'] = 'Группа пользователя';
$MESS['PROGRAM_TABLE_ENTITY_PERCENT_FIELD'] = 'Процент начисления (от суммы заказа)';
$MESS['PROGRAM_TABLE_ENTITY_MAX_PAY_PERCENT_FIELD'] = 'Доля баллов от суммы заказа при оплате (не более %)';
$MESS['PROGRAM_TABLE_ENTITY_POST_USER_FIELD'] = 'Отправлять пользователю уведомление о начислении';
$MESS['PROGRAM_TABLE_ENTITY_POST_ADMIN_FIELD'] = 'Отправлять администратору уведомление о начислении';

