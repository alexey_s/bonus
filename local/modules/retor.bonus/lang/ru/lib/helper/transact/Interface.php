<?php
/**
 * Created by PhpStorm.
 * User: devaccess
 * Date: 15.07.17
 * Time: 20:15
 */

$MESS['TRANSACT_TABLE_ENTITY_ID_FIELD'] = 'ID';
$MESS['TRANSACT_TABLE_ENTITY_USER_ID_FIELD'] = 'Пользователь';
$MESS['TRANSACT_TABLE_ENTITY_TIMESTAMP_FIELD'] = 'Время изменения записи';
$MESS['ACCOUNTS_TABLE_ENTITY_TRANSACT_DATE_FIELD'] = 'Дата транзакции';
$MESS['ACCOUNTS_TABLE_ENTITY_BONUS_FIELD'] = 'Начислено бонусов';
$MESS['ACCOUNTS_TABLE_ENTITY_ORDER_SUM_FIELD'] = 'Сумма заказа';
$MESS['ACCOUNTS_TABLE_ENTITY_CURRENCY_FIELD'] = 'Валюта';
$MESS['ACCOUNTS_TABLE_ENTITY_DEBIT_FIELD'] = 'Вид операции';
$MESS['ACCOUNTS_TABLE_ENTITY_ORDER_ID_FIELD'] = 'Номер заказа';
$MESS['ACCOUNTS_TABLE_ENTITY_DESCRIPTION_FIELD'] = 'Тип операции';
$MESS['ACCOUNTS_TABLE_ENTITY_NOTES_FIELD'] = 'Описание';
$MESS['ACCOUNTS_TABLE_ENTITY_PAYMENT_ID_FIELD'] = 'Платежная система';
$MESS['ACCOUNTS_TABLE_ENTITY_EMPLOYEE_ID_FIELD'] = 'Покупатель';
