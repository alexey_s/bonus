<?php

namespace retor\bonus\Model;


use Bitrix\Main\Entity\DataManager;
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

class TransactTable extends DataManager
{
    public static function getTableName()
    {
        return 'retor_bonus_transact_table';
    }

    public static function getMap()
    {
        $fieldsMap = array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
            ),
            'USER_ID' => array(
                'data_type' => 'integer',
                'required' => true,
            ),
            'TIMESTAMP_X' => array(
                'data_type' => 'datetime',
                'required' => true,
            ),
            'TRANSACT_DATE' => array(
                'data_type' => 'datetime',
                'required' => true,
            ),
            'BONUS' => array(
                'data_type' => 'float',
                'required' => true,
            ),
            'ORDER_SUM' => array(
                'data_type' => 'float',
                'required' => true,
            ),
            'CURRENCY' => array(
                'data_type' => 'string',
            ),
            'DEBIT' => array(
                'data_type' => 'boolean',
                'values' => array('N', 'Y'),
            ),
            'ORDER_ID' => array(
                'data_type' => 'integer',
            ),
            'DESCRIPTION' => array(
                'data_type' => 'string',
            ),
            'NOTES' => array(
                'data_type' => 'text',
            ),
            'PAYMENT_ID' => array(
                'data_type' => 'integer',
            ),
            'EMPLOYEE_ID' => array(
                'data_type' => 'integer',
            ),
        );

        return $fieldsMap;
    }
}