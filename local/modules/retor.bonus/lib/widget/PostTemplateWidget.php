<?php
/**
 * Created by PhpStorm.
 * User: devaccess
 * Date: 19.07.17
 * Time: 21:13
 */

namespace retor\bonus\Widget;

use Bitrix\Main\Localization\Loc;
use retor\bonus\Helper\AdminEditHelper;
use retor\bonus\Helper\AdminListHelper;
use retor\bonus\Helper\AdminSectionListHelper;

Loc::loadMessages(__FILE__);

class PostTemplateWidget extends HelperWidget
{


    protected function AddDropDownField($id, $arSelect, $value=false, $arParams=array())
    {

        $html = '<select name="'.$id.'"';
        foreach($arParams as $param)
            $html .= ' '.$param;
        $html .= '>';

        foreach($arSelect as $key => $val)
            $html .= '<option value="'.htmlspecialcharsbx($key).'"'.($value == $key? ' selected': '').'>'.htmlspecialcharsex($val).'</option>';
        $html .= '</select>';

        return $html;

    }

    protected function getEditHtml()
    {

        $arPostList = array();
        $arFilter = array();
        $rsPosts = \CEventMessage::GetList($by="site_id", $order="desc", $arFilter);
        while ($arPost = $rsPosts->GetNext())
        {
            $arPostList[$arPost['ID']] = '('.$arPost['ID'].') '.$arPost['SUBJECT'];
        }


        return self::AddDropDownField("SITE_ID",$arPostList);

    }

    public function generateRow(&$row, $data)
    {

    }

    public function showFilterHtml()
    {

    }

}

?>