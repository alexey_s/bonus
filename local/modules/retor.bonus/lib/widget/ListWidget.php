<?php
/**
 * Created by PhpStorm.
 * User: devaccess
 * Date: 19.07.17
 * Time: 21:13
 */

namespace retor\bonus\Widget;

use Bitrix\Main\Localization\Loc;
use retor\bonus\Helper\AdminBaseHelper;
use retor\bonus\Helper\AdminEditHelper;
use retor\bonus\Helper\AdminListHelper;
use retor\bonus\Helper\AdminSectionListHelper;

Loc::loadMessages(__FILE__);

class ListWidget extends HelperWidget
{
    protected function getEditHtml()
    {
        $arGroupList = array();
        $arGroupList = unserialize($this->getValue());
        ob_start();
        ?>
        <script>
            function addRow(){
                var ele = $('table#range_list tr:last').clone();
                $(ele).find('input').val('0');
                $('table#range_list tr:last').after(ele);
            }
        </script>
        <table id="range_list" class="internal" style="width: auto;" border="0" cellspacing="0" cellpadding="0">
            <tbody>
                <tr class="heading">
                    <td align="center">Сумма оплаченных заказов</td>
                    <td align="center">Величина скидки</td>
                </tr>
                <? if (!empty($arGroupList)) {
                    foreach ($arGroupList as $key => $group) { ?>
                        <tr>
                            <td>от (не менее) <input name="SUM[]" size="13" value="<?= $group["SUM"]; ?>" type="text"></td>
                            <td><input name="PERCENTS[]" size="13" value="<?= $group["PERCENT"]; ?>" type="text">
                                <select name="TYPE[]" style="width:150px;">
                                    <option value="P" selected="">В процентах</option>
                                    <option value="F">Фиксированная сумма</option>
                                </select>
                            </td>
                        </tr>
                    <?
                    }
                } else { ?>
                    <tr>
                        <td>от (не менее) <input name="SUM[]" size="13" value="0" type="text"></td>
                        <td><input name="PERCENTS[]" size="13" value="0" type="text">
                            <select name="TYPE[]" style="width:150px;">
                                <option value="P" selected="">В процентах</option>
                                <option value="F">Фиксированная сумма</option>
                            </select>
                        </td>
                    </tr>
                 <? } ?>
            </tbody>
        </table>

        <div style="width: 100%; text-align: left; margin-top: 10px;">
            <input class="adm-btn-big" onclick="addRow();" type="button" value="Еще" title="Еще">
        </div>
        <input type="hidden" name="RANGES_COUNT" id="RANGES_COUNT" value="<?= intval($intCount); ?>">
        <?
        return ob_get_clean();
    }

    protected function getMultipleEditHtml()
    {
        $style = $this->getSettings('STYLE');
        $size = $this->getSettings('SIZE');
        $uniqueId = $this->getEditInputHtmlId();

        $rsEntityData = null;

        if (!empty($this->data['ID'])) {
            $entityName = $this->entityName;
            $rsEntityData = $entityName::getList(array(
                'select' => array('REFERENCE_' => $this->getCode() . '.*'),
                'filter' => array('=ID' => $this->data['ID'])
            ));
        }

        ob_start();
        ?>

        <div id="<?= $uniqueId ?>-field-container" class="<?= $uniqueId ?>">
        </div>

        <script>
            var multiple = new MultipleWidgetHelper(
                '#<?= $uniqueId ?>-field-container',
                '{{field_original_id}}<input type="text" name="<?= $this->getCode()?>[{{field_id}}][<?=$this->getMultipleField('VALUE')?>]" style="<?=$style?>" size="<?=$size?>" value="{{value}}">'
            );
            <?
            if ($rsEntityData)
            {
            while($referenceData = $rsEntityData->fetch())
            {
            if (empty($referenceData['REFERENCE_' . $this->getMultipleField('ID')])) {
                continue;
            }

            ?>
            multiple.addField({
                value: '<?= static::prepareToJs($referenceData['REFERENCE_' . $this->getMultipleField('VALUE')]) ?>',
                field_original_id: '<input type="hidden" name="<?= $this->getCode()?>[{{field_id}}][<?= $this->getMultipleField('ID') ?>]"' +
                ' value="<?= $referenceData['REFERENCE_' . $this->getMultipleField('ID')] ?>">',
                field_id: <?= $referenceData['REFERENCE_' . $this->getMultipleField('ID')] ?>
            });
            <?
            }
            }
            ?>

            // TODO Добавление созданных полей
            multiple.addField();
        </script>
        <?
        return ob_get_clean();
    }


    public function processEditAction()
    {
        $percents = array();

        foreach ($this->data['PERCENTS'] as $key => $percent) {
            if (empty($percent) && empty($this->data['SUM'][$key])) {
                continue;
            }
            $percents[] = array(
                'PERCENT' => $percent,
                'SUM' => $this->data['SUM'][$key],
                'TYPE' => $this->data['TYPE'][$key]
            );

        }

        $this->setValue(serialize($percents));


        parent::processEditAction();

    }


    public function generateRow(&$row, $data)
    {
        $html = '';

        $arProgramList = unserialize($data[$this->code]);

        foreach ($arProgramList as $program) {
            $html .= 'Процент: ' . $program['PERCENT'] . '<br>';
            $html .= 'Сумма: ' . $program['SUM'] . '<br>';
        }

        $row->AddViewField($this->code, $html);

    }

    public function showFilterHtml()
    {

    }

}

?>