<?php

use Bitrix\Main\Localization\Loc;
use retor\bonus\Helper\AdminBaseHelper;
use retor\bonus\Widget\NumberWidget;
use retor\bonus\Widget\DateTimeWidget;
use retor\bonus\Widget\UserWidget;
use retor\bonus\Widget\StringWidget;
use retor\bonus\Widget\TextAreaWidget;
use retor\bonus\Widget\ComboBoxWidget;

Loc::loadMessages(__FILE__);

AdminBaseHelper::setInterfaceSettings(
    array(
        'FIELDS' => array(
            'ID' => array(
                'WIDGET' => new NumberWidget(),
                'TITLE' => Loc::getMessage('TRANSACT_TABLE_ENTITY_ID_FIELD'),
            ),
            'USER_ID' => array(
                'WIDGET' => new UserWidget(),
                'TITLE' => Loc::getMessage('TRANSACT_TABLE_ENTITY_USER_ID_FIELD'),
            ),
            'TIMESTAMP_X' => array(
                'WIDGET' => new DateTimeWidget(),
                'TITLE' => Loc::getMessage('TRANSACT_TABLE_ENTITY_TIMESTAMP_FIELD'),
            ),
            'TRANSACT_DATE' => array(
                'WIDGET' => new DateTimeWidget(),
                'TITLE' => Loc::getMessage('ACCOUNTS_TABLE_ENTITY_TRANSACT_DATE_FIELD'),
            ),
            'BONUS' => array(
                'WIDGET' => new StringWidget(),
                'TITLE' => Loc::getMessage('ACCOUNTS_TABLE_ENTITY_BONUS_FIELD'),
            ),
            'ORDER_SUM' => array(
                'WIDGET' => new StringWidget(),
                'TITLE' => Loc::getMessage('ACCOUNTS_TABLE_ENTITY_ORDER_SUM_FIELD'),
            ),
            'CURRENCY' => array(
                'WIDGET' => new StringWidget(),
                'TITLE' => Loc::getMessage('ACCOUNTS_TABLE_ENTITY_CURRENCY_FIELD'),
            ),
            'DEBIT' => array(
                'WIDGET' => new ComboBoxWidget(),
                'TITLE' => Loc::getMessage('ACCOUNTS_TABLE_ENTITY_DEBIT_FIELD'),
            ),
            'ORDER_ID' => array(
                'WIDGET' => new NumberWidget(),
                'TITLE' => Loc::getMessage('ACCOUNTS_TABLE_ENTITY_ORDER_ID_FIELD'),
            ),
            'DESCRIPTION' => array(
                'WIDGET' => new StringWidget(),
                'TITLE' => Loc::getMessage('ACCOUNTS_TABLE_ENTITY_DESCRIPTION_FIELD'),
            ),
            'NOTES' => array(
                'WIDGET' => new TextAreaWidget(),
                'TITLE' => Loc::getMessage('ACCOUNTS_TABLE_ENTITY_NOTES_FIELD'),
            ),
            'PAYMENT_ID' => array(
                'WIDGET' => new NumberWidget(),
                'TITLE' => Loc::getMessage('ACCOUNTS_TABLE_ENTITY_PAYMENT_ID_FIELD'),
            ),
            'EMPLOYEE_ID' => array(
                'WIDGET' => new UserWidget(),
                'TITLE' => Loc::getMessage('ACCOUNTS_TABLE_ENTITY_EMPLOYEE_ID_FIELD'),
            ),
        )
    ),
    array(
        'retor\bonus\Helper\transact\TransactEditHelper',
        'retor\bonus\Helper\transact\TransactListHelper'
    ),
    'retor.bonus'
);