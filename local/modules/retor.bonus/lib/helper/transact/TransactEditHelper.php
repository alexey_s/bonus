<?php

namespace retor\bonus\Helper\transact;


use retor\bonus\Helper\AdminEditHelper;

class TransactEditHelper extends AdminEditHelper
{
    static protected $model = 'retor\bonus\Model\TransactTable';
    static public $module = 'retor.bonus';
    static protected $listViewName = 'transact_list';
    static protected $viewName = 'transact_detail';
}