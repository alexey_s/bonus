<?php

namespace retor\bonus\Helper\transact;

use retor\bonus\Helper\AdminListHelper;

class TransactListHelper extends AdminListHelper
{
    static protected $model = 'retor\bonus\Model\TransactTable';
    static public $module = 'retor.bonus';
    static protected $viewName = 'transact_list';
    static protected $editViewName = 'transact_detail';
}