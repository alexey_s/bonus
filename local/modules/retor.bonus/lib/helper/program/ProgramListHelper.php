<?php

namespace retor\bonus\Helper\program;

use retor\bonus\Helper\AdminListHelper;

class ProgramListHelper extends AdminListHelper
{
    static protected $model = 'retor\bonus\Model\ProgramTable';
    static public $module = 'retor.bonus';
    static protected $viewName = 'program_list';
    static protected $editViewName = 'program_detail';
}