<?php

namespace retor\bonus\Helper\program;


use retor\bonus\Helper\AdminEditHelper;

class ProgramEditHelper extends AdminEditHelper
{
    static protected $model = 'retor\bonus\Model\ProgramTable';
    static public $module = 'retor.bonus';
    static protected $listViewName = 'program_list';
    static protected $viewName = 'program_detail';
    //static protected $routerUrl = '/bitrix/admin/settings.php?lang=ru&mid=retor.bonus&mid_menu=1';

    public function getFields()
    {
        $fields = $this->fields;
        unset($fields['SUM']);
        unset($fields['TYPE']);
        return $fields;
    }

    protected function saveElement($id = null)
    {
        /** @var EntityManager $entityManager */
        unset($this->data['SUM']);
        unset($this->data['TYPE']);
        $entityManager = new static::$entityManager(static::getModel(), empty($this->data) ? array() : $this->data, $id, $this);
        $saveResult = $entityManager->save();
        $this->addNotes($entityManager->getNotes());

        return $saveResult;
    }


}