<?php

namespace retor\bonus\Helper\table;

use retor\bonus\Helper\AdminListHelper;

class TableListHelper extends AdminListHelper
{
    static protected $model = 'retor\bonus\Model\AccountsTable';
    static public $module = 'retor.bonus';
    static protected $viewName = 'table_list';
    static protected $editViewName = 'table_detail';
}