<?php
/**
 * Created by PhpStorm.
 * User: devaccess
 * Date: 30.07.17
 * Time: 14:15
 *
 * Класс для работы с бонусным счетом
 */

use retor\bonus\EntityManager;
use retor\bonus\Helper\AdminBaseHelper;
use retor\bonus\Model\TransactTable;


class CBonusTransact
{

    //Создает новую транзакцию
    public static function Add($arFields)
    {

        $module = \retor\bonus\Helper\transact\TransactEditHelper::$module;
        $view = \retor\bonus\Helper\transact\TransactListHelper::getViewName();


        list($helper, $interface) = AdminBaseHelper::getGlobalInterfaceSettings($module, $view);

        $fields = isset($interface['FIELDS']) ? $interface['FIELDS'] : array();
        $tabs = isset($interface['TABS']) ? $interface['TABS'] : array();
        $isPopup = "N";
        $adminHelper = new $helper($fields, $isPopup);
        $bonusManager = new EntityManager(
            'retor\bonus\Model\TransactTable',
            array(
                'USER_ID' => $arFields['USER_ID'],
                'TIMESTAMP_X' => new \Bitrix\Main\Type\Datetime($arFields['Order']['DATE_UPDATE'],"Y-m-d H:i:s"),
                'TRANSACT_DATE' => new \Bitrix\Main\Type\Datetime($arFields['Order']['DATE_UPDATE'],"Y-m-d H:i:s"),
                'BONUS' => $arFields['BONUS_SUM'],
                'ORDER_SUM' => $arFields['Order']['PRICE'],
                'CURRENCY' => 'RUB',
                'DEBIT' => $arFields['DEBT'],
                'ORDER_ID' => $arFields['Order']['ID'],
                'DESCRIPTION' => $arFields['Desc'],
                'NOTES' => $arFields['Notes'],
            ),
            null,
            $adminHelper
        );

        $result = $bonusManager->save();

        return $result;

    }

    public static function AddExt($arFields)
    {
        global $DB;
        $err_mess = "";

        $InsertStr = $DB->PrepareInsert("retor_bonus_transact_table", $arFields, null,false);

        $DB->StartTransaction();
        $strSql = "INSERT INTO retor_bonus_transact_table (".$InsertStr[0].") VALUES (".$InsertStr[1].")";
        $ID = $DB->Query($strSql, false, $err_mess.__LINE__);

        $ID = intval($ID);

        if (strlen($err_mess)<=0)
        {
            $DB->Commit();
        }

        else $DB->Rollback();

        if($err_mess){
            return false;
        }else{
            return true;
        }
    }

    //Выбирает транзакции пользователя
    public static function GetList($UserId)
    {
        global $DB;
        $UserId = (int)$UserId;

        $strSql = "SELECT * FROM retor_bonus_transact_table UA WHERE UA.USER_ID = " . $UserId . " ORDER BY UA.TRANSACT_DATE DESC";

        $dbUserTransact = $DB->Query($strSql, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);
        while($Transact = $dbUserTransact->GetNext()) {

            $arTransact['TRANSACT'][] = $Transact;

        }

        $USER_ACCOUNT = CBonusAccount::getAccountByUserID($UserId);
        $arTransact['BONUS_COUNT'] = $USER_ACCOUNT['BONUS_COUNT'];
        return $arTransact;
    }


}