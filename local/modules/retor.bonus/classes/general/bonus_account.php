<?php
/**
 * Created by PhpStorm.
 * User: devaccess
 * Date: 30.07.17
 * Time: 14:15
 *
 * Класс для работы с бонусным счетом
 */

use retor\bonus\EntityManager;
use retor\bonus\Helper\AdminBaseHelper;
use retor\bonus\Model\AccountsTable;


class CBonusAccount
{

    public static function GetBonusSettings($USER_ID,$SITE_ID)
    {
        global $DB;

        $settings = array();
        $programs = array();

        $bonus_active = COption::GetOptionString("retor.bonus", "retor_bonus_active", "N");

        $arGroups = CUser::GetUserGroup($USER_ID); // массив групп, в которых состоит пользователь

        $strSql = "SELECT * FROM retor_bonus_program_table UA WHERE UA.ACTIVE = 'Y' AND UA.SITE_ID = '" . $SITE_ID . "'";

        $dbPrograms = $DB->Query($strSql, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);
        while ($program = $dbPrograms->Fetch()) {
            $program['USER_GROUP_ID'] = unserialize($program['USER_GROUP_ID']);
            $program['PERCENTS'] = unserialize($program['PERCENTS']);

            $result_intersect = array_intersect($program['USER_GROUP_ID'], $arGroups);// далее проверяем, если пользователь вошёл хотя бы в одну из групп, то позволяем ему что-либо делать

            if (!empty($result_intersect)) {
                $programs[] = $program;
            }

        }

        $settings['bonus_active'] = $bonus_active;
        $settings['site_id'] = $SITE_ID;
        $settings['programs'] = $programs;

        return $settings;

    }

    public static function getAccountByUserID($UserId)
    {
        global $DB;
        $UserId = (int)$UserId;

        $strSql = "SELECT * FROM retor_bonus_accounts_table UA WHERE UA.USER_ID = " . $UserId;

        $dbUserAccount = $DB->Query($strSql, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);
        if ($arAccount = $dbUserAccount->Fetch()) {
            return $arAccount;
        }

        return null;

    }

    function getTotalOrderSum($UserId)
    {
        $dblPrice = 0;

        $rsSaleOrders = CSaleOrder::GetList(array(), Array("USER_ID" => $UserId, "PAYED" => "Y"), false, false, array('ID', 'PRICE', 'CURRENCY', 'DATE_INSERT'));
        while ($arSaleOrder = $rsSaleOrders->Fetch()) {
            $dblPrice += $arSaleOrder['PRICE'];
        }

        return $dblPrice;
    }

    //Создает новый счет пользователя или обновляет существующий
    public static function Add($arFields)
    {

        $ID = $arFields['ID'];

        $module = \retor\bonus\Helper\table\TableEditHelper::$module;
        $view = \retor\bonus\Helper\table\TableListHelper::getViewName();


        list($helper, $interface) = AdminBaseHelper::getGlobalInterfaceSettings($module, $view);

        $fields = isset($interface['FIELDS']) ? $interface['FIELDS'] : array();
        $tabs = isset($interface['TABS']) ? $interface['TABS'] : array();
        $isPopup = "N";

        $adminHelper = new $helper($fields, $isPopup);

        $bonusManager = new EntityManager(
            'retor\bonus\Model\AccountsTable',
            $arFields,
            $ID,
            $adminHelper
        );



        $result = $bonusManager->save();

        return $result;

    }

    //Создает новый счет пользователя или обновляет существующий
    public static function AddExt($arFields)
    {
        global $DB;
        $err_mess = "";

        $ID = $arFields['ID'];
        unset($arFields['ID']);

        $DB->PrepareFields("retor_bonus_accounts_table");

        $DB->StartTransaction();

        if ($ID > 0)
        {
            $strUpdate = $DB->PrepareUpdate("retor_bonus_accounts_table", $arFields, "form");
            $strSql = "UPDATE retor_bonus_accounts_table SET ".$strUpdate." WHERE ID=".$ID;
            $DB->Query($strSql, false, $err_mess.__LINE__);
        }
        else
        {
            $ID = $DB->Insert("retor_bonus_accounts_table", $arFields, $err_mess.__LINE__);
        }

        $ID = intval($ID);

        if (strlen($err_mess)<=0)
        {

            $DB->Commit();
        }
        else $DB->Rollback();

        if($err_mess){
            return false;
        }else{
            return true;
        }

    }

    //Получает максимальный процент по программе (если SUM = 0 - процент начислится всегда. накопительный процент ссумируется с ним)
    function getTotalProcentByProgram($percents, $TotalSum)
    {

        $totalPercent = 0;


        uasort($percents, function ($a, $b) {
            return $b['SUM'] - $a['SUM'];
        });

        $maxPercent = 0;
        foreach ($percents as $percent) {

            if ($percent['SUM'] == 0) {
                $totalPercent += $percent['PERCENT'];
            }elseif($TotalSum > $percent['SUM'] && $maxPercent < $percent['PERCENT']) {
                $maxPercent = $percent['PERCENT'];
            }
        }
        $totalPercent += $maxPercent;

        return $totalPercent;

    }


    //Функция "При оплате" - зачисляет баллы на бонусный счет при оплате заказа (статус "Выполнен")
    public static function PayIn($arFields)
    {

        if($arFields['OrderStatus'] == 'F'){

            $Account = self::getAccountByUserID($arFields['USER_ID']); //получаем аккаунт пользователя
            $TotalSum = self::getTotalOrderSum($arFields['USER_ID']); //сумма всех оплаченных заказов

            foreach ($arFields['BonusProgram'] as $program) {
                $BonusPercent = self::getTotalProcentByProgram($program['PERCENTS'], $arFields['Order']['PRICE']);

                if($BonusPercent > 0) {

                        $BONUS_CREDIT = round((intval($arFields['Order']['PRICE']) / 100) * intval($BonusPercent));

                    if ($Account == null) {
                        $BONUS_COUNT = $BONUS_CREDIT;
                        $arBonusFields = array(
                            'USER_ID' => $arFields['USER_ID'],
                            'USER_GROUP_ID' => 2,
                            'PERCENT' => $BonusPercent,
                            'BONUS_COUNT' => $BONUS_COUNT,
                            'SALE_AMOUNT' => $TotalSum,
                            'MODIFIED' => new \Bitrix\Main\Type\Datetime($arFields['Order']['DATE_UPDATE'], "Y-m-d H:i:s"), //@TO-DO не верный формат при записи
                        );
                    } else {
                        $BONUS_COUNT = $Account['BONUS_COUNT'] + $BONUS_CREDIT;
                        $Account['BONUS_COUNT'] = $BONUS_COUNT;
                        $Account['SALE_AMOUNT'] = $TotalSum;
                        $Account['PERCENT'] = $BonusPercent;
                        $Account['MODIFIED'] = new \Bitrix\Main\Type\Datetime($arFields['Order']['DATE_UPDATE'], "Y-m-d H:i:s");
                        $arBonusFields = $Account;
                    }

                    if ($arBonusFields) {
                        $res = \CBonusAccount::Add($arBonusFields);
                        $arFields['BONUS_SUM'] = $BONUS_CREDIT;
                        $arFields['Desc'] = 'CREDIT';
                        $arFields['Notes'] = 'Начисление баллов';
                        $arFields['DEBT'] = 'N';
                        CBonusTransact::Add($arFields);
                    }


                    //Почтовые события
                    $sendUser =  intval(COption::GetOptionString("retor.bonus", "retor_bonus_send_user"));
                    $sendAdmin =  intval(COption::GetOptionString("retor.bonus", "retor_bonus_send_admin"));

                    $rsUser = CUser::GetByID($arFields['USER_ID']);
                    $arUser = $rsUser->Fetch();

                    $arEventFields = array(
                        "USERNAME" => $arUser['LAST_NAME'].' '.$arUser['NAME'],
                        "BONUS_DATE" => date("Y-m-d H:i:s"),
                        "BONUS_SUM" => $Account['BONUS_COUNT'],
                        "BONUS_INC" => $BONUS_CREDIT,
                        "ORDER_ID" => $arFields['Order']["ID"],
                        "ORDER_DATE" => $arFields['Order']["DATE_INSERT"],
                        "ORDER_SUM" => $arFields['Order']['PRICE'],
                    );

                    if($sendUser == "Y") {
                        //Отправка письма
                        $arEventFields["EMAIL_TO"] = $arUser["EMAIL"];
                        CEvent::Send("RETOR_BONUS_INC_BALL", $arFields['Order']['LID'], $arEventFields);
                    }

                    if($sendAdmin == "Y") {
                        //Отправка письма
                        $arEventFields["EMAIL_TO"] = COption::GetOptionString("main", "email_from");
                        CEvent::Send("RETOR_BONUS_INC_BALL",  $arFields['Order']['LID'], $arEventFields);
                    }
                    //

                }

            }

        }

        $result = $res;
    }

    //Расчет возможной суммы оплаты с бонусного счета
    public static function getBonusRemain($ORDER_ID,$BONUS_NEED,$REPEAT_DEBIT = false){
        $arOrder = CSaleOrder::GetByID($ORDER_ID);
        $MAX_PERCENT = intval(COption::GetOptionString("retor.bonus", "retor_bonus_max_percent"));

        $Account = self::getAccountByUserID($arOrder['USER_ID']); //получаем аккаунт пользователя



        //Если нет счета или баланс нулевой
        if(!$Account || $Account['BONUS_COUNT'] <= 0){
            return false;
        }else {

            // Если не возможна многократная оплата с бонусного счета проверяем был
            // ли до этого предоплачен заказ
            if (!$REPEAT_DEBIT) {

                if ($arOrder['SUM_PAID'] > 0) {
                    return false;
                }
            }

            $ORDER_PRICE = $arOrder['PRICE']; //сумма заказа

            //Оверпэй
            if($Account['BONUS_COUNT'] < $BONUS_NEED){
                $BONUS_NEED = $Account['BONUS_COUNT'];
            }

            $BONUS_NEED_PERCENT = ($BONUS_NEED * 100) / $ORDER_PRICE;

            if ($BONUS_NEED_PERCENT > $MAX_PERCENT) {
                $BONUS_NEED = ($ORDER_PRICE * $MAX_PERCENT) / 100; // максимально возможная сумма списания с бонусного счета ($MAX_PERCENT)
            }

        }

        return $BONUS_NEED;
    }

    function doPayOrder($ORDER_ID,$PAY_SUM){
        $paySystemId =  intval(COption::GetOptionString("retor.bonus", "retor_bonus_pay_id"));


        $order = \Bitrix\Sale\Order::load($ORDER_ID);
        $extpaySystemId = $order->getField("PAY_SYSTEM_ID");

        $paymentCollection = $order->getPaymentCollection();

        $paySystemObject = \Bitrix\Sale\PaySystem\Manager::getObjectById($paySystemId);

        /** @var \Bitrix\Sale\Payment $payment */
        $payment = $paymentCollection->createItem($paySystemObject);
        $payment->setFields(array(
            'SUM' => $PAY_SUM,
            'CURRENCY'=> $order->getCurrency(),
        ));

        $order->doFinalAction(true);

        $payment->setPaid("Y"); // "N"
        $result = $order->save();

        $order = \Bitrix\Sale\Order::load($ORDER_ID);
        $paymentCollection = $order->getPaymentCollection();

        $paySystemId =  $extpaySystemId;
        $paySystemObject = \Bitrix\Sale\PaySystem\Manager::getObjectById($paySystemId);

        $remainingSum = $order->getPrice() - $paymentCollection->getSum();

        /** @var \Bitrix\Sale\Payment $payment */
        $payment = $paymentCollection->createItem($paySystemObject);
        $payment->setFields(array(
            'SUM' => $remainingSum,
            'CURRENCY'=> $order->getCurrency(),
        ));

        $order->doFinalAction(true);

        //$payment->setPaid("N"); // "N"
        $result = $order->save();


        if (!$result->isSuccess()) {
            return false;
        }else{
            return true;
        }
    }

    //Функция "Оплата бонусами" - списывает баллы при оплате бонусами
    public static function PayOut($ORDER_ID,$BOUNS_DEBT,$EXT = false)
    {
        $order = CSaleOrder::GetByID($ORDER_ID);
        $Account = self::getAccountByUserID($order['USER_ID']); //получаем аккаунт пользователя

        if($Account){
            $TotalSum = self::getTotalOrderSum($order['USER_ID']); //сумма всех оплаченных заказов
            $Account['BONUS_COUNT'] =  $Account['BONUS_COUNT'] - $BOUNS_DEBT;
            $Account['SALE_AMOUNT'] = $TotalSum;
            $Account['MODIFIED'] = new \Bitrix\Main\Type\Datetime(date("Y-m-d H:i:s"), "Y-m-d H:i:s");

            if(!$EXT) {
                $arFields = array(
                    'USER_ID' => $Account['USER_ID'],
                    'BONUS_SUM' => $BOUNS_DEBT,
                    'ORDER_ID' => $ORDER_ID,
                    'Order' => $order['PRICE'],
                    'Desc' => 'DEBET',
                    'Notes' => 'Списание баллов',
                    'DEBT' => 'Y'
                );

                $is_payd = self::doPayOrder($ORDER_ID,$BOUNS_DEBT);
                if($is_payd){
                    $res = self::Add($Account);
                    CBonusTransact::Add($arFields);
                }

            }else{
                $arFields = array(
                    'USER_ID' => $Account['USER_ID'],
                    'TIMESTAMP_X' => new \Bitrix\Main\Type\Datetime(date("Y-m-d H:i:s"), "Y-m-d H:i:s"),
                    'TRANSACT_DATE' => new \Bitrix\Main\Type\Datetime(date("Y-m-d H:i:s"), "Y-m-d H:i:s"),
                    'BONUS' => $BOUNS_DEBT,
                    'ORDER_SUM' => $order['PRICE'],
                    'CURRENCY' => $order['CURRENCY'],
                    'DEBIT' => 'N',
                    'ORDER_ID' => $ORDER_ID,
                    'DESCRIPTION' => 'DEBET',
                    'NOTES' => 'Списание баллов',
                    'PAYMENT_ID' => $order['PAY_SYSTEM_ID'],
                    'EMPLOYEE_ID' => $Account['USER_ID'],
                );

                $is_payd = self::doPayOrder($ORDER_ID,$BOUNS_DEBT);

                if($is_payd){
                    $res = self::AddExt($Account);
                    $result = CBonusTransact::AddExt($arFields);
                }


            }


            $sendUser =  intval(COption::GetOptionString("retor.bonus", "retor_bonus_send_user"));
            $sendAdmin =  intval(COption::GetOptionString("retor.bonus", "retor_bonus_send_admin"));

            $rsUser = CUser::GetByID($order['USER_ID']);
            $arUser = $rsUser->Fetch();

            $arEventFields = array(
                "USERNAME" => $arUser['LAST_NAME'].' '.$arUser['NAME'],
                "BONUS_DATE" => date("Y-m-d H:i:s"),
                "BONUS_SUM" => $Account['BONUS_COUNT'],
                "BONUS_DEC" => $BOUNS_DEBT,
                "ORDER_ID" => $order["ID"],
                "ORDER_DATE" => $order["DATE_INSERT"],
                "ORDER_SUM" => $order['PRICE'],
            );

            if($sendUser == "Y") {
                //Отправка письма
                $arEventFields["EMAIL_TO"] = $arUser["EMAIL"];
                CEvent::Send("RETOR_BONUS_DEC_BALL", $order['LID'], $arEventFields);
            }

            if($sendAdmin == "Y") {
                //Отправка письма
                $arEventFields["EMAIL_TO"] = COption::GetOptionString("main", "email_from");
                CEvent::Send("RETOR_BONUS_DEC_BALL",  $order['LID'], $arEventFields);
            }


            return $result;
        }else{
            return false;
        }

    }

}