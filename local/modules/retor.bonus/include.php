<?php

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

Loader::registerAutoLoadClasses('retor.bonus',
    array(
        //Admin Helper part
        'retor\bonus\EventHandlers' => 'lib/EventHandlers.php',

        'retor\bonus\Helper\Exception' => 'lib/helper/Exception.php',

        'retor\bonus\Helper\AdminInterface' => 'lib/helper/AdminInterface.php',
        'retor\bonus\Helper\AdminBaseHelper' => 'lib/helper/AdminBaseHelper.php',
        'retor\bonus\Helper\AdminListHelper' => 'lib/helper/AdminListHelper.php',
        'retor\bonus\Helper\AdminSectionListHelper' => 'lib/helper/AdminSectionListHelper.php',
        'retor\bonus\Helper\AdminEditHelper' => 'lib/helper/AdminEditHelper.php',
        'retor\bonus\Helper\AdminSectionEditHelper' => 'lib/helper/AdminSectionEditHelper.php',

        'retor\bonus\EntityManager' => 'lib/EntityManager.php',

        'retor\bonus\Widget\HelperWidget' => 'lib/widget/HelperWidget.php',
        'retor\bonus\Widget\CheckboxWidget' => 'lib/widget/CheckboxWidget.php',
        'retor\bonus\Widget\ComboBoxWidget' => 'lib/widget/ComboBoxWidget.php',
        'retor\bonus\Widget\StringWidget' => 'lib/widget/StringWidget.php',
        'retor\bonus\Widget\ListWidget' => 'lib/widget/ListWidget.php',
        'retor\bonus\Widget\NumberWidget' => 'lib/widget/NumberWidget.php',
        'retor\bonus\Widget\FileWidget' => 'lib/widget/FileWidget.php',
        'retor\bonus\Widget\TextAreaWidget' => 'lib/widget/TextAreaWidget.php',
        'retor\bonus\Widget\HLIBlockFieldWidget' => 'lib/widget/HLIBlockFieldWidget.php',
        'retor\bonus\Widget\DateTimeWidget' => 'lib/widget/DateTimeWidget.php',
        'retor\bonus\Widget\IblockElementWidget' => 'lib/widget/IblockElementWidget.php',
        'retor\bonus\Widget\UrlWidget' => 'lib/widget/UrlWidget.php',
        'retor\bonus\Widget\VisualEditorWidget' => 'lib/widget/VisualEditorWidget.php',
        'retor\bonus\Widget\UserWidget' => 'lib/widget/UserWidget.php',
        'retor\bonus\Widget\UserGroupWidget' => 'lib/widget/UserGroupWidget.php',
        'retor\bonus\Widget\OrmElementWidget' => 'lib/widget/OrmElementWidget.php',
        'retor\bonus\Widget\SiteWidget' => 'lib/widget/SiteWidget.php',
        'retor\bonus\Widget\PostTemplateWidget' => 'lib/widget/PostTemplateWidget.php',

        //

        //bonus part

        'retor\bonus\Helper\table\TableListHelper' => "lib/helper/table/TableListHelper.php",
        'retor\bonus\Helper\table\TableEditHelper' => "lib/helper/table/TableEditHelper.php",
        'retor\bonus\Model\AccountsTable' => "lib/model/AccountsTable.php",

        //transact part

        'retor\bonus\Helper\transact\TransactListHelper' => "lib/helper/transact/TransactListHelper.php",
        'retor\bonus\Helper\transact\TransactEditHelper' => "lib/helper/transact/TransactEditHelper.php",
        'retor\bonus\Model\TransactTable' => "lib/model/TransactTable.php",

        //options programm

        'retor\bonus\Helper\program\ProgramListHelper' => "lib/helper/program/ProgramListHelper.php",
        'retor\bonus\Helper\program\ProgramEditHelper' => "lib/helper/program/ProgramEditHelper.php",
        'retor\bonus\Model\ProgramTable' => "lib/model/ProgramTable.php",

        //main classes

        //'retor\bonus\Account\Account' => "lib/bonus/account/Account.php",

    )
);

CModule::AddAutoloadClasses(
    "retor.bonus",
    array(
        "CBonusAccount" => "classes/general/bonus_account.php",
        "CBonusTransact" => "classes/general/bonus_transact.php",
    )
);

global $APPLICATION;
$isAdminPage = strpos($APPLICATION->GetCurDir(), 'bitrix/admin') !== false;
if ($isAdminPage) {
    Loader::includeModule('retor.bonus');
    require_once(__DIR__ . '/lib/helper/table/Interface.php');
    require_once(__DIR__ . '/lib/helper/program/Interface.php');
    require_once(__DIR__ . '/lib/helper/transact/Interface.php');
}
