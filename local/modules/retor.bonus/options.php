<?php

use Bitrix\Main\Application;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Text\String;


defined('ADMIN_MODULE_NAME') or define('ADMIN_MODULE_NAME', 'retor.bonus');

if (!$USER->isAdmin()) {
    $APPLICATION->authForm('Nope');
}

$app = Application::getInstance();
$context = $app->getContext();
$request = $context->getRequest();

$SET = array(
    'retor_bonus_active' => ($_REQUEST['retor_bonus_active'] == 'on') ? 'Y' : 'N',
    'site' => $_REQUEST['site'],
    'retor_bonus_discount' => $_REQUEST['retor_bonus_discount'],
    'retor_bonus_max_percent' => $_REQUEST['retor_bonus_max_percent'],
    'retor_bonus_pay_id' => $_REQUEST['retor_bonus_pay_id'],
    'retor_bonus_send_user' => ($_REQUEST['retor_bonus_send_user'] == 'on') ? 'Y' : 'N',
    'retor_bonus_send_admin' => ($_REQUEST['retor_bonus_send_admin'] == 'on') ? 'Y' : 'N',
);

Loc::loadMessages($context->getServer()->getDocumentRoot()."/bitrix/modules/main/options.php");
Loc::loadMessages(__FILE__);

$aTabs[] = array("DIV" => "edit1", "TAB" => GetMessage('MAIN_TAB_SET'), "ICON" => "", "TITLE" => GetMessage('MAIN_TAB_TITLE_SET'));
//$aTabs[] = array("DIV" => "edit2", "TAB" => GetMessage('RETOR_BONUS_PROGRAMM'), "ICON" => "", "TITLE" => GetMessage('RETOR_BONUS_PROGRAMM'));
$tabControl = new CAdminTabControl("tabControl", $aTabs);

if ((!empty($save) || !empty($restore)) && $request->isPost() && check_bitrix_sessid()) {
    if (!empty($restore)) {
        Option::delete(ADMIN_MODULE_NAME);
        CAdminMessage::showMessage(array(
            "MESSAGE" => Loc::getMessage("REFERENCES_OPTIONS_RESTORED"),
            "TYPE" => "OK",
        ));
    } elseif(!empty($save)) {

    foreach($SET as $name => $value){
        Option::set(
            ADMIN_MODULE_NAME,
            $name,
            $value
        );
    }

        CAdminMessage::showMessage(array(
            "MESSAGE" => Loc::getMessage("RETOR_BONUS_OPTIONS_SAVED"),
            "TYPE" => "OK",
        ));
    } else {
        CAdminMessage::showMessage(Loc::getMessage("RETOR_BONUS_INVALID_VALUE"));
    }
}

$tabControl->begin();
?>

<form method="post" action="<?=sprintf('%s?mid=%s&lang=%s', $request->getRequestedPage(), urlencode($mid), LANGUAGE_ID)?>">
    <?php
    echo bitrix_sessid_post();
    $tabControl->beginNextTab();

    $RETOR_BONUS_ACTIVE = COption::GetOptionString(ADMIN_MODULE_NAME, "retor_bonus_active") == 'Y'? true: false;
    $RETOR_BONUS_MAX_PERCENT = COption::GetOptionString(ADMIN_MODULE_NAME, "retor_bonus_max_percent");
    $RETOR_BONUS_PAY_SYSTEM_ID = COption::GetOptionString(ADMIN_MODULE_NAME, "retor_bonus_pay_id");
    $RETOR_BONUS_SEND_USER = COption::GetOptionString(ADMIN_MODULE_NAME, "retor_bonus_send_user") == 'Y'? true: false;
    $RETOR_BONUS_SEND_ADMIN = COption::GetOptionString(ADMIN_MODULE_NAME, "retor_bonus_send_admin") == 'Y'? true: false;

    $RETOR_BONUS_USE_DISCOUNT = array(
      "onGoods" => GetMessage("RETOR_BONUS_ON_GOODS"),
      "onOrder" => GetMessage("RETOR_BONUS_ON_ORDER")
    );

    $obSite = CSite::GetList($by="sort", $order="desc");
	$arSites = array();
	while($arResult = $obSite->Fetch()) {
        $arSites[$arResult['ID']] = $arResult['NAME'];
    }

    ?>
    <tr class="heading">
        <td colspan="2"><?=GetMessage("RETOR_BONUS_OPTIONS")?></td>
    </tr>

    <tr>
        <td width="40%">
            <label for="retor_bonus_active"><?=GetMessage("RETOR_BONUS_ACTIVE")?>:</label>
        </td>
        <td width="60%">
            <input type="checkbox" name="retor_bonus_active" <?=($RETOR_BONUS_ACTIVE?'checked="checked"' :'')?> />
        </td>
    </tr>

    <tr>
        <td width="40%">
            <label for="retor_bonus_site"><?=GetMessage("RETOR_BONUS_SITE")?>:</label>
        </td>
        <td width="60%">
            <select name="site" id='bonussite' onchange='javascript:changeSite(this.value);'>
                <? foreach($arSites as $id => $value):?>
                    <option value="<?=$id?>" <?=((COption::GetOptionString(ADMIN_MODULE_NAME, 'site') == $id))?' selected="selected"':''?>><?=$value?></option>
                <? endforeach;?>
            </select>
        </td>
    </tr>

    <tr>
        <td width="40%">
            <label for="retor_bonus_send_user"><?=GetMessage("RETOR_BONUS_SEND_USER")?>:</label>
        </td>
        <td width="60%">
            <input type="checkbox" name="retor_bonus_send_user" <?=($RETOR_BONUS_SEND_USER?'checked="checked"' :'')?> />
        </td>
    </tr>

    <tr>
        <td width="40%">
            <label for="retor_bonus_send_admin"><?=GetMessage("RETOR_BONUS_SEND_ADMIN")?>:</label>
        </td>
        <td width="60%">
            <input type="checkbox" name="retor_bonus_send_admin" <?=($RETOR_BONUS_SEND_ADMIN?'checked="checked"' :'')?> />
        </td>
    </tr>



    <tr class="heading">
        <td colspan="2"><?=GetMessage("RETOR_BONUS_RULES")?></td>
    </tr>

    <tr>
        <td width="40%">
            <label for="retor_bonus_active"><?=GetMessage("RETOR_BONUS_MAX_PERCENT")?>:</label>
        </td>
        <td width="60%">
            <input type="text" name="retor_bonus_max_percent" value="<?=$RETOR_BONUS_MAX_PERCENT;?>" />
        </td>
    </tr>

    <tr>
        <td width="40%">
            <label for="retor_bonus_pay_id"><?=GetMessage("RETOR_BONUS_PAY_SYSTEM_ID")?>:</label>
        </td>
        <td width="60%">
            <input type="text" name="retor_bonus_pay_id" value="<?=$RETOR_BONUS_PAY_SYSTEM_ID;?>" />
        </td>
    </tr>



    <?php

    $tabControl->buttons();
    ?>
    <input type="submit"
           name="save"
           value="<?=Loc::getMessage("MAIN_SAVE") ?>"
           title="<?=Loc::getMessage("MAIN_OPT_SAVE_TITLE") ?>"
           class="adm-btn-save"
           />
    <input type="submit"
           name="restore"
           title="<?=Loc::getMessage("MAIN_HINT_RESTORE_DEFAULTS") ?>"
           onclick="return confirm('<?= AddSlashes(GetMessage("MAIN_HINT_RESTORE_DEFAULTS_WARNING")) ?>')"
           value="<?=Loc::getMessage("MAIN_RESTORE_DEFAULTS") ?>"
           />
    <?php
    ?>
    <? $tabControl->end(); ?>
</form>

<?
$tabControl->BeginNextTab();?>
<?require_once("options_loyality.php");?>










