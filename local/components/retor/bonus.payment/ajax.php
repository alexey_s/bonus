<?php
define('STOP_STATISTICS', true);
define('NO_KEEP_STATISTIC', 'Y');
define('NO_AGENT_STATISTIC','Y');
define('DisableEventsCheck', true);
define('BX_SECURITY_SHOW_MESSAGE', true);

require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

//	$BONUS_NEED = $_REQUEST['BONUS_NEED'];
//	$ORDER_ID = $_REQUEST['ORDER_ID'];
//
//	if($BONUS_NEED > 0 && $ORDER_ID > 0){
//		$Remain = CBonusAccount::getBonusRemain($ORDER_ID,$BONUS_NEED);
//
//		if($Remain){
//
//			$result = CBonusAccount::PayOut($ORDER_ID,$Remain,true);
//
//		}
//	}
//
//echo json_encode(array('result' => $result));

$request = Bitrix\Main\Application::getInstance()->getContext()->getRequest();
$request->addFilter(new \Bitrix\Main\Web\PostDecodeFilter);

if (!Bitrix\Main\Loader::includeModule('retor.bonus'))
	return;

CBitrixComponent::includeComponentClass('retor:bonus.payment');

$component = new CBonusPaymentsComponent();
$component->executeComponent();

?>

