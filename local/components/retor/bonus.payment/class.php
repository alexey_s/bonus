<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main,
    Bitrix\Main\Loader,
    Bitrix\Main\Localization\Loc,
    Bitrix\Sale;
//global $USER;
//
//$arResult = array();
//
//$CanPay = false;
//
//$ORDER_ID = $arParams['ORDER_ID'];
//
//$arResult['ACCOUNT'] = $Account = CBonusAccount::getAccountByUserID($USER->GetID()); //получаем аккаунт пользователя
//$ORDER = \CSaleOrder::GetByID($ORDER_ID);
//
//if($ORDER['SUM_PAID'] == 0)
//{
//    $CanPay = true;
//}
//
//
//$RETOR_BONUS_MAX_PERCENT = COption::GetOptionString("retor.bonus", "retor_bonus_max_percent");
//$half_price = ($ORDER['PRICE'] * intval($RETOR_BONUS_MAX_PERCENT)) / 100;
//
//$arResult['AVALIBLE_SUM'] = CBonusAccount::getBonusRemain($ORDER_ID,$half_price);
//
//$arResult['ORDER'] = $ORDER;
//$arResult['CanPay'] = $CanPay;
//
//
//$this->IncludeComponentTemplate();

if (!Loader::includeModule("sale"))
{
    ShowError(Loc::getMessage("SOA_MODULE_NOT_INSTALL"));
    return;
}

class CBonusPaymentsComponent extends CBitrixComponent
{
    protected $orderId;
    protected $context;

    public function executeComponent()
    {
        global $APPLICATION;

        $this->context = Main\Application::getInstance()->getContext();
        $isAjaxRequest = $this->request["is_ajax_post"] == "Y";

        if ($isAjaxRequest)
            $APPLICATION->RestartBuffer();

        $basket = Sale\Basket::loadItemsForFUser(CSaleBasket::GetBasketUserID(), $this->context->getSite());

       // var_dump($basket); exit;

        $this->includeComponentTemplate();

        if ($isAjaxRequest)
        {
            $APPLICATION->FinalActions();
            die();
        }
    }

    /**
     * Prepare Component Params.
     *
     * @param array $params Component parameters.
     * @return array
     */
    public function onPrepareComponentParams($params)
    {

    }
}

?>

